//
//  AppConstant.swift
//  TheMovieDb
//
//  Created by User on 6/7/20.
//  Copyright © 2020 aturan23. All rights reserved.
//

import Foundation
import UIKit

class AppConstants {
    static let statusBarHeight = UIApplication.shared.statusBarFrame.height
    static let screenHeight = UIScreen.main.bounds.height
    static let screenWidth = UIScreen.main.bounds.width
    static let navBarHeight = UINavigationController().navigationBar.bounds.height
}
