//
//  ViewController.swift
//  TheMovieDb
//
//  Created by User on 6/7/20.
//  Copyright © 2020 aturan23. All rights reserved.
//

import UIKit

fileprivate var activityView: UIView?

extension UIViewController {
    func showSpinner() {
        activityView = UIView(frame: self.view.bounds)
        activityView?.backgroundColor = UIColor(red: 0.5, green: 0.5, blue: 0.5, alpha: 0)
        let activityIndicatorView = UIActivityIndicatorView(style: .whiteLarge)
        activityIndicatorView.center = activityView!.center
        activityIndicatorView.startAnimating()
        activityView?.addSubview(activityIndicatorView)
        self.view.addSubview(activityView!)
    }
    
    func removeSpinner() {
        activityView?.removeFromSuperview()
        activityView = nil
    }
}

