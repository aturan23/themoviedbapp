//
//  String.swift
//  TheMovieDb
//
//  Created by User on 6/7/20.
//  Copyright © 2020 aturan23. All rights reserved.
//

import Foundation
extension String {
    func serverUrlString() -> String {
        
        return "https://image.tmdb.org/t/p/w500"+self
    }
    func inUrl() -> URL {
        
        return URL(string: self)!
    }
}

 
