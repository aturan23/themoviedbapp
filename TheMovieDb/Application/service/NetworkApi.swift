//
//  NetworkApi.swift
//  TheMovieDb
//
//  Created by User on 6/6/20.
//  Copyright © 2020 aturan23. All rights reserved.
//

import Foundation
import PromiseKit
import Alamofire

class NetworkApi {
    
    private init() {}
    
    static func api(rest: Rest, method: HTTPMethod, params: [String: Any]) -> Promise<Any> {
        return Promise { seal in
            let serverParams: [String: Any] = ["Content-Type": "application/json; charset=utf-8"]
            let params = serverParams.merging(params, uniquingKeysWith: { (first, _) in first })
            AF.request("\(rest.rest())?api_key=74af438c96a31d4fe2917f326835f6fa",
                    method: method,
                    parameters: params).validate()
                .responseJSON(completionHandler: {
                    (response) in
                    switch response.result {
                    case .success(let success):
                        seal.fulfill(success)
                    case .failure(let e):
                        seal.reject(e)
                    }
                })
        }
    }

}
