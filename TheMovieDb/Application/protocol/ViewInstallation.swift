//
//  ViewInstallation.swift
//  TheMovieDb
//
//  Created by User on 6/6/20.
//  Copyright © 2020 aturan23. All rights reserved.
//

import Foundation

protocol ViewInstallation {
    func addSubviews()
    func addConstraints()
    func stylizeViews()
}

extension ViewInstallation {
    func setupViews() {
        addSubviews()
        addConstraints()
        stylizeViews()
    }
}
