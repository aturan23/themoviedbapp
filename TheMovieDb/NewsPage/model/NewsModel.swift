//
//  News.swift
//  TheMovieDb
//
//  Created by User on 6/6/20.
//  Copyright © 2020 aturan23. All rights reserved.
//

import Foundation

class NewsModel {
    var id: Int?
    var poster: String?
    var title: String?
    var release: String?
    var vote: Float?
    var overview: String?
    
    init?(dictList: [String: Any]) {
        self.id = dictList["id"] as? Int
        self.poster = dictList["poster_path"] as? String
        self.title = dictList["title"] as? String
        self.release = dictList["release_date"] as? String
        self.vote = dictList["vote_average"] as? Float
        self.overview = dictList["overview"] as? String
    }
}
