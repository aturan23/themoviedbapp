//
//  NewsTableCell.swift
//  TheMovieDb
//
//  Created by User on 6/6/20.
//  Copyright © 2020 aturan23. All rights reserved.
//

import UIKit
import Kingfisher

class NewsTableViewCell: UITableViewCell {
    
    var titleLabel = UILabel()
    var posterView = UIImageView()
    var voteLabel = UILabel()
    var releaseLabel = UILabel()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func configuration(model: NewsModel) -> Void {
        titleLabel.text = model.title
        voteLabel.text = String(describing: model.vote ?? 0.0)
        releaseLabel.text = model.release
        if let poster = model.poster {
            posterView.kf.setImage(with: poster.serverUrlString().inUrl())
        }
    }
}

extension NewsTableViewCell: ViewInstallation {
    func addSubviews() {
        addSubview(titleLabel)
        addSubview(posterView)
        addSubview(releaseLabel)
        addSubview(voteLabel)
        
    }
    
    func addConstraints() {
        
        posterView.snp.makeConstraints { (make) in
            make.width.height.equalTo(60)
            make.top.equalTo(8)
            make.bottom.equalTo(-8)
            make.left.equalTo(16)
        }
        
        releaseLabel.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview()
            make.left.equalTo(posterView.snp.right).offset(16)
        }
        
        titleLabel.snp.makeConstraints { (make) in
            make.left.equalTo(posterView.snp.right).offset(16)
            make.right.equalTo(8)
            make.bottom.equalTo(releaseLabel.snp.top).offset(-4)
        }
        
        voteLabel.snp.makeConstraints { (make) in
            make.left.right.equalTo(titleLabel)
            make.top.equalTo(releaseLabel.snp.bottom).offset(4)
        }
        
    }
    
    func stylizeViews() {
        posterView.layer.cornerRadius = 30
        posterView.layer.masksToBounds = true
        
        titleLabel.font = UIFont.boldSystemFont(ofSize: 16.0)
        
        releaseLabel.textColor = .darkGray
        releaseLabel.font = UIFont.systemFont(ofSize: 14)
        
        voteLabel.textColor = .darkGray
        voteLabel.font = UIFont.systemFont(ofSize: 14)
        
    }
    
    
}
