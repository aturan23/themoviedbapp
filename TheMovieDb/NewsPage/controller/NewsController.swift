//
//  NewsController.swift
//  TheMovieDb
//
//  Created by User on 6/6/20.
//  Copyright © 2020 aturan23. All rights reserved.
//

import UIKit

class NewsController: UIViewController {
    var tableView = UITableView()
    
    private var viewModel = NewsViewModal();

    override func viewDidLoad() {
        super.viewDidLoad()
        loadData()
        setupViews()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        navigationController?.navigationBar.prefersLargeTitles = true
    }

    @objc private func loadData() {
        self.showSpinner()
        viewModel.getList().done { [weak self] in
            self?.tableView.reloadData()
            self?.removeSpinner()
            self?.tableView.refreshControl?.endRefreshing()
            }.catch { (e) in
                self.removeSpinner()
                print("Error => ", e.localizedDescription)
        }
    }
}

extension NewsController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.newsList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: NewsTableViewCell.cellIdentifier(), for: indexPath) as! NewsTableViewCell
        
        cell.configuration(model: viewModel.newsList[indexPath.row])
        
        return cell
    }
    
    // pagination
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == self.viewModel.newsList.count - 3 {
            self.viewModel.getListByParams().done({ (result) in
                if result {
                    tableView.reloadData()
                }
            }).catch { (e) in
                print(e.localizedDescription)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = NewsDetailViewController(news: viewModel.newsList[indexPath.row])
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension NewsController: ViewInstallation {
    
    func addSubviews() {
        view.addSubview(tableView)
    }
    
    func addConstraints(){
        tableView.snp.makeConstraints { (make) in
            make.left.right.bottom.equalToSuperview()
            make.top.equalTo(AppConstants.navBarHeight + 8)
        }
    }
    
    func stylizeViews() {
        navigationItem.title = "News"
        view.backgroundColor = .white
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(NewsTableViewCell.self, forCellReuseIdentifier: NewsTableViewCell.cellIdentifier())
        
        tableView.refreshControl = UIRefreshControl()
        tableView.refreshControl?.backgroundColor = UIColor(ciColor: .clear)
        tableView.refreshControl?.tintColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
        tableView.refreshControl?.addTarget(self, action: #selector(loadData), for: .valueChanged)
    }
}
