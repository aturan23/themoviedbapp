//
//  NewsViewModal.swift
//  TheMovieDb
//
//  Created by User on 6/6/20.
//  Copyright © 2020 aturan23. All rights reserved.
//

import Foundation
import PromiseKit

class NewsViewModal {
    var newsList = [NewsModel]()
    var page = 1
    var total = 10
    
    func getList() -> Promise<Void> {
        return Promise { [weak self] seal in
            NewsApi.getList(page: page).done { (newsList) in
                    self?.newsList = []
                    self?.newsList = newsList
                    seal.fulfill(())
                }.catch { (e) in
                    seal.reject(e)
            }
        }
    }
    
    func getListByParams() -> Promise<Bool> {
        return Promise { [weak self] seal in
            if (page <= total) {
                NewsApi.getList(page: page + 1).done { (newsList) in
                        self?.page += 1
                        for item in newsList {
                            self?.newsList.append(item)
                        }
                        seal.fulfill(true)
                    }.catch { (e) in
                        seal.reject(e)
                }
            } else {
                seal.fulfill(false)
            }
        }
    }
}
