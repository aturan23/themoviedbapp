//
//  NewsApi.swift
//  TheMovieDb
//
//  Created by User on 6/6/20.
//  Copyright © 2020 aturan23. All rights reserved.
//

import Foundation
import Alamofire
import PromiseKit

class NewsApi {
    
    private init() {}
    
    static func getList(page: Int) -> Promise<[NewsModel]> {
        return Promise { seal in
            let params: [String: Any] = ["page": page]
            NetworkApi.api(rest: .GETLIST, method: .get, params: params).done({ (response) in
                guard let responseModel = response as? [String: Any] else { throw AppError.defError }
                guard let resultList = responseModel["results"] as? [[String: Any]] else { throw AppError.defError }
                var modelList = [NewsModel]()
                for item in resultList {
                    guard let model = NewsModel(dictList: item) else { continue }
                    modelList.append(model)
                }
                seal.fulfill(modelList)
            }).catch({ (e) in
                seal.reject(e)
            })
        }
    }
}
